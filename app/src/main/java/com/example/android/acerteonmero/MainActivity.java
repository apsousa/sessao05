package com.example.android.acerteonmero;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private int numero;
    TextView acertos;
    private static int numAcertos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        acertos = (TextView) findViewById(R.id.tv_acertos);

        numero = geradorNumero(10);
        Toast.makeText(this, "Gerado número: " + numero, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        numero = geradorNumero(10);
        Toast.makeText(this, "Gerado número: " + numero, Toast.LENGTH_SHORT).show();

        acertos.setText("Acertos consecutivos " + numAcertos);
    }

    public void btnClicado(View v) {

        Intent intent = new Intent(this, MostraResultado.class);
        boolean resultado;

        // obter id da vista
        if( v.getId() == R.id.bt_menorigual) {
            resultado = numero <= 5;
        } else {
            resultado = numero > 5;
        }


        intent.putExtra("resultado", resultado);
        startActivity(intent);

        if( resultado ) {
            numAcertos++;

        } else {
            numAcertos = 0;
        }

    }

    private int geradorNumero(int limite) {
        return new Random().nextInt(limite);
    }

}
