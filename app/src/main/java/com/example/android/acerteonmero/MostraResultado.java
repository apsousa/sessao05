package com.example.android.acerteonmero;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class MostraResultado extends AppCompatActivity {

    ImageView imagem;
    TextView  mensagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostra_resultado);

        imagem = (ImageView) findViewById(R.id.iv_imagem);
        mensagem = (TextView) findViewById(R.id.tv_certoerrado);

        // obter resposta via intent
        boolean resposta = getIntent().getBooleanExtra("resultado", true);

        if( resposta ) {
            imagem.setImageResource(R.drawable.certo);
            mensagem.setText("Acertou!");
        } else {
            imagem.setImageResource(R.drawable.errado);
            mensagem.setText("Errou!");
        }


    }
}
